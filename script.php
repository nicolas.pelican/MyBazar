<?php
	if (isset($argv[1])) {
		$controller = strtolower($argv[1]);
		mkdir('module/pages/'.$controller);
		touch('module/pages/'.$controller.'/index.php');
		$current = file_get_contents('public/lib/SampleController.php'); 
		$replace = str_replace("Sample", ucfirst($controller).'Controller', $current);
		file_put_contents('module/src/Controller/'.ucfirst($controller).'Controller.php', $replace);
	} else {
		echo "Error";
	}
?>