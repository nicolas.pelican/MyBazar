<span class="row-fluid">
    <form id="register-form" class="form form-horizontal" action="<?= $this->url('User', 'add'); ?>" method='POST'>
        <div class="control-group">
            <label class="control-label" for="firstname">Firstname : </label>
            <input  class="input-block-level" id="firstname" name="firstname" type="text"><br/>
        </div>
        <div class="control-group">
            <label class="control-label" for="lastname">Lastname : </label>
            <input  class="input-block-level" id="lastname" name="lastname" type="text"><br/>
        </div>
        <div class="control-group">
            <label class="control-label" for="email">Email : </label>
            <input  class="input-block-level" id="email" name="email" type="email"><br/>
        </div>
         <div class="control-group">
            <label class="control-label" for="email">Login : </label>
            <input  class="input-block-level" id="login" name="login" type="text"><br/>
        </div>
        <div class="control-group">
            <label class="control-label" for="password">Password : </label>
            <input  class="input-block-level" name="password"  onpaste="return false;"   type="password"><br/>
        </div>
        <div class="control-group"> 
            <label class="control-label" for="confirm_password">Confirm password: </label>   
            <input  class="input-block-level" id="confirm_password"  onpaste="return false;"   name="confirm_password" type="password"><br/>
        </div>
        <div class="control-group"> 
            <input type="checkbox" class="check_invalid" id="check_invalid" name="check">
            <div id="submit">
                <button type="submit" class="btn btn-success">Register</button><br/>
            </div>
        </div>
    </form>
</span>




