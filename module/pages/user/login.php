
<span class="row-fluid">
<form id="loginForm" method="post" action="<?= $this->url('Auth', 'logIn') ?>" class="form-horizontal">
    <div class="form-group">
        <label class="col-xs-3 control-label">Login</label>
        <div class="col-xs-5">
            <input type="text" class="form-control" autofocus name="login" required />
        </div>
    </div>

    <div class="form-group">
        <label class="col-xs-3 control-label">Password</label>
        <div class="col-xs-5">
            <input type="password" class="form-control" autofocus name="password" required  />
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-9 col-xs-offset-3">
            <button type="submit" id="formsubmit" class="btn btn-primary">Sign in</button>
        </div>
    </div>
</form>
</span>
