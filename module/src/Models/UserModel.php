<?php
class UserModel extends Model{

    public function getUsers() {
      	$user = $this->getAll('user');
        return $user;
    }
     
    public function getUser() {
      	$user = $this->getBy('user');
        return $user;
    }

    public function addUser(array $user) {
    	$random_id = random_int(100, 999);
    	while ($this->isUnique('user_id', $random_id)) {
    		$random_id = random_int(100, 999);
    	}
    	$user_id = $random_id;
    	echo '<pre>'.$user_id.'</pre>';
    	$array = array('firstname', 'lastname', 'email', 'login', 'password');
    	$stmt = $this->dbh->prepare("INSERT INTO $this->table (user_id, firstname, lastname, email, login, password) VALUE(:user_id, :firstname, :lastname, :email, :login, :password)");
    	$stmt->bindValue(':user_id', $user_id);    
    	foreach ($user as $field => $value) {
    		if(in_array($field, $array)){
    			if ($field == 'password') {
    				$value = $this->hashPassword($value);
    			}
    			$stmt->bindValue(':'.$field, $value);
    		}
    	}
    	
    	try {
    		$result = $stmt->execute();
    		echo '<pre>';
    	$arr = $stmt->errorInfo();
    	} catch( PDOException $Exception ) {
		    // Note The Typecast To An Integer!
		    throw new MyDatabaseException( $Exception->getMessage( ) , (int)$Exception->getCode( ) );
		    //return $Exception->getMessage( );
		}
		return $result;
		
    }

    private function isUnique(string $field, int $str) {
    	$sql =  $this->dbh->query("SELECT $field FROM $this->table WHERE $field = $str");
		if ($sql->fetchColumn() < 1) {
			return false;
		}
    	return true;
    }

    private function hashPassword($pass) {
    	$options = [
    		'salt' => md5('myBazar'),
		];
		return password_hash($pass, PASSWORD_DEFAULT, $options)."\n";
    }
}