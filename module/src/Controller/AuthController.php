<?php

use CORE\AbstractController;
use CORE\Error;

class AuthController extends AbstractController{
	
	function index(){
        $this->set(array());
		$html =$this->render(__FUNCTION__);
	}

	function logIn(){
		//Look if form was post
		if (count($_POST) < 1) {
				/*array_push($this->error, 'Form is invalid');*/
				$this->error->setMsg($key.'Form is invalid');
		}

		//Look if every post field was empty
		foreach ($_POST as $key => $value) {
			if (!isset($_POST[$key]{0})) {
				$this->error->setMsg($key.' field is empty');
				//array_push($this->error, $key.' field is empty');
			}	
		}
		
		if (!empty($this->error->getMsg())) {
			$this->dispatch('User', 'login');
		}

		$login = $_POST['login'];
		$password = $_POST['password'];
			
		$authModel = new AuthModel("user");
        $result = $authModel->loginIsValid($login, $password);

    	if ($result) {
        	$this->dispatch('User', 'index');
        } else  {

        	$this->error->setMsg("Invalid Login/password combinaison");
        	//array_push($this->error, "Invalid Login/password combinaison");  	
        }
		$this->dispatch('User', 'login');
		exit();
	}

	function register() {
		$error = array();

		//Look if form was post
		if (count($_POST) < 1) {
			array_push($error, 'Form is invalid');
		}

		$error = $this->error->checkFieldEmpty($_POST);

		if (count($error) > 0) {
			$this->dispatch('User', 'register', $error);
		}

		$error = $this->error->checkLenght($_POST['login'], 3);
		$error = $this->error->checkLenght($_POST['password'], 8);
		
		if (count($error) > 0) {
			$this->dispatch('User', 'register', $error);
		}
		
		/*if (isset($_POST['login']{3})) {
			array_push($error, $key.' field is empty');
		}*/

		$login = $_POST['login'];
		$password = $_POST['password'];
			
    	if ($result) {
        	$this->dispatch('User', 'index', '');
        } else  {
        	array_push($error, "Invalid Login/password combinaison");
        	
        }
		$this->dispatch('User', 'register', $error);
		exit();
	}
}

?>