<?php

use CORE\AbstractController;

class TestController extends AbstractController{
	
	function index(){
		$d['tuto']= array('nom' => 'pelican', 'prenom'=>'nicolas');
		$this->set($d);
		$this->render(__FUNCTION__);
	}
}