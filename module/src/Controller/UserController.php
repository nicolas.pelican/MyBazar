<?php

use CORE\AbstractController;
use CORE\Error;

class UserController extends AbstractController{
	
	function index(){

        $userModel = new UserModel("user");

        $users = $userModel->getUsers();
       
        $this->set(
        	array(
        		'users' => $users
        	)
        );
		$html = $this->render(__FUNCTION__);
		return $html;
	}

	function login(){
		$this->set(
        	array('error' => $this->error)
        );
		$html =$this->render(__FUNCTION__);	
		return $html;
	}

	function add() {
		//Look if form was post
		if (count($_POST) < 1) {
			$this->error->setMsg($key.'Form is invalid');
		}

		$error = $this->error->checkFieldEmpty($_POST);

		if (!empty($this->error->getMsg())) {
			$this->dispatch('User', 'register', $error);
		}

		$this->error->checkLenght($_POST['login'], 2);
		$this->error->checkLenght($_POST['password'], 7);
		
		if(!$this->error->confirmPassword($_POST['password'], $_POST['confirm_password'])) {
			$this->error->setMsg('Password are different');
		}

		if (!empty($this->error->getMsg())) {
			$this->dispatch('User', 'register', $error);
		}
		$userModel = new UserModel("user");

        $result = $userModel->addUser($_POST);
		
    	/*if ($result) {
        	$this->dispatch('User', 'index', '');
        } else  {
        	array_push($error, "Invalid Login/password combinaison");
        	
        }*/
        $this->dispatch('User', 'index', '');
		exit();
	}

	function register() {
		var_dump($_POST);
		$this->set(	
        	array('error' => $this->error)
        );
		$html =$this->render(__FUNCTION__);	
		return $html;
	}
}

?>