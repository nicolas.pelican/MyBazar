<?php
	return array(
    'db' => array(
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=mybazar;hostname=localhost',
        'host' => 'localhost',
        'dbname' => 'mybazar',
        'username' => 'root',
        'password' => '',
        'port' => '',
        'charset' => '',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
    ),
);