<?php

try {
    $hote               = 'localhost';
    $baseDeDonnees      = 'mybazar';
    $identifiant        = '';
    $motDePasse         = '';
  
    $bdd = new PDO( 'mysql:host=' . $hote . ';dbname=' . $baseDeDonnees, $identifiant, $motDePasse, array( PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION ) );
} catch( Exception $e ) {
    die( 'Erreur : ' . $e->getMessage() );
}

?>