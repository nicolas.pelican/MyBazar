<?php
class Framework {

    private $config;

    private static $instance;


    private function __construct()
    {
        self::init();

        self::autoload();
        error_log('construct');
    }


    public static function run() {

        error_log("run()");

        if( !isset( self::$instance))
        {
            $obj = __CLASS__;
            self::$instance = new $obj;
            error_log('$instance');      
        }
        //return self::$instance;
        self::dispatch();
        //require ROOT. "public/template/template.php";
        //self::layout($html);
    }


    private static function init() {
    // Define path constants

    define("DS", DIRECTORY_SEPARATOR);
    define("PS", PATH_SEPARATOR);
    
    $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,strpos( $_SERVER["SERVER_PROTOCOL"],'/'))).'://';

    //define('PROTOCOL', strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,strpos( $_SERVER["SERVER_PROTOCOL"],'/'))).'://';
    define('PROTOCOL', $protocol);

    //define('PUBLIC_PATH',$_SERVER['HTTP_HOST']. DS . 'MyBazar' . DS . 'public' . DS);
    //define('PUBLIC_PATH',$_SERVER['HTTP_HOST']. DS . 'public' . DS);

    define('PUBLIC_PATH', $_SERVER['HTTP_HOST']. '/' . 'MyBazar' . '/' . 'public' . '/');

   


    define("ROOT", getcwd() . DS);

    // define('PUBLIC_PATH', ROOT. 'public' . '/');

    define('WEBROOT', PROTOCOL .  $_SERVER['HTTP_HOST']. '/' . 'MyBazar' . '/');

    //define('WEBROOT', $_SERVER['HTTP_HOST']. '/');


    define("APP_PATH", ROOT . 'module' . DS);

    define("CONFIG_PATH", ROOT . 'config' . DS);

    define("FRAMEWORK_PATH", ROOT . "framework" . DS);

   // define("PUBLIC_PATH", ROOT . "public" . DS);


    //define("CONFIG_PATH", APP_PATH . "config" . DS);

    define("SRC_PATH", APP_PATH . "src" . DS);

    define("CONTROLLER_PATH", APP_PATH . "src" . DS . "Controller" . DS);

    define("MODEL_PATH", SRC_PATH . "Models" . DS);

    define("VIEW_PATH", APP_PATH . "views" . DS);

    define("PAGE_PATH", APP_PATH . "pages" . DS);


    define("CORE_PATH", FRAMEWORK_PATH . "core" . DS);

    define('DB_PATH', FRAMEWORK_PATH . "database" . DS);

    define("LIB_PATH", FRAMEWORK_PATH . "libraries" . DS);

    define("HELPER_PATH", FRAMEWORK_PATH . "helpers" . DS);

    define("UPLOAD_PATH", PUBLIC_PATH . "uploads" . DS);

    define("IMAGE_PATH", $protocol . PUBLIC_PATH . "images" . '/');

    // Define platform, controller, action, for example:

    // index.php?p=admin&c=Goods&a=add

    define("PLATFORM", isset($_REQUEST['p']) ? $_REQUEST['p'] : 'home');

    define("CONTROLLER", isset($_REQUEST['c']) ? $_REQUEST['c'] : 'User');

    define("ACTION", isset($_REQUEST['a']) ? $_REQUEST['a'] : 'index');

    define("CURR_CONTROLLER_PATH", CONTROLLER_PATH);

    //define("CURR_CONTROLLER_PATH", CONTROLLER_PATH . PLATFORM . DS);

    //define("CURR_VIEW_PATH", VIEW_PATH . PLATFORM . DS);

    define("CURR_VIEW_PATH", PAGE_PATH . CONTROLLER_PATH);


     // Load configuration file

    $config = include CONFIG_PATH . "config.php";

    //var_dump($this->config);
    // Load core classes

    require CORE_PATH . "Controller.class.php";

    require CORE_PATH . "Loader.class.php";

    require CORE_PATH . "AbstractController.php";

    require CORE_PATH . "Template.class.php";

    require CORE_PATH . "Model.class.php";

    require CORE_PATH . "Mypdo.class.php";

    //require CORE_PATH . "Error.php";

    //die(CORE_PATH . "Error.php");
   // require CORE_PATH . "Route.class.php";

    /*require DB_PATH . "Mysql.class.php";

    require CORE_PATH . "Model.class.php";*/

    // Start session

    session_start();
    }

    // Autoloading

    private static function autoload(){

        spl_autoload_register(array(__CLASS__,'load'));

    }


    // Define a custom load method



    private static function load($classname){


        // Here simply autoload app’s controller and model classes

        if (substr($classname, -10) == "Controller"){

            // Controller
            require_once CURR_CONTROLLER_PATH . "$classname.php";
            //require_once CURR_CONTROLLER_PATH . "$classname.class.php";

        } elseif (substr($classname, -5) == "Model"){

            // Model
            require_once  MODEL_PATH . "$classname.php";
            //require_once  MODEL_PATH . "$classname.class.php";
        } else {
            require CORE_PATH . "Error.php";
        }


    }


    private static function dispatch() {
        // Instantiate the controller class and call its action method
        $controller_name = CONTROLLER . "Controller";

        $action_name = ACTION;


        $path = explode('/', $_SERVER['REQUEST_URI']);
 
        $controller_name = (isset($path[2]{0}) && !empty($path[2]))? $path[2]. "Controller" : 'User';

        $action_name = (isset($path[3]{0}) && !empty($path[3]))? $path[3] : 'login';

        $controller = new $controller_name;

        $content = $controller->$action_name();
        require ROOT. "public/template/template.php";
    }

    private static function layout($html) {
       /* $template = new Template();
        $template->set($html);
        $template->render();*/

    }

}