<?php

class Model 
{
    protected $dbh; //database connection object

    protected $table; //table name

    protected $fields = array();  //fields list

    public function __construct($table){

        $config = include CONFIG_PATH . "config.php";

    	$dbconfig['dsn']	= $config['db']['dsn'];

        $dbconfig['host'] = $config['db']['host'];

        $dbconfig['user'] = $config['db']['username'];

        $dbconfig['password'] = $config['db']['password'];

        $dbconfig['dbname'] = $config['db']['dbname'];

        $dbconfig['port'] = $config['db']['port'];

        $dbconfig['charset'] = $config['db']['charset'];

        $this->table = $table;

        $this->dbh = Mypdo::getInstance($dbconfig);
    }

   protected function getAll($table)
   {
		$request = $this->dbh->prepare("SELECT * FROM $this->table");
		//$this->db->query($request, $table);
        $this->dbh->execute($request);
		$result = $this->dbh->fetchAll($request);
		return  $result;
   }
}

?>