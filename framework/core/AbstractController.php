<?php

namespace CORE;

class AbstractController {

	var $vars = array();

	protected $error;

	protected $param;

	public function __construct() {
		 $this->error = new Error();
	}


	function set($d) 
	{
       $this->vars = array_merge($this->vars, $d);
    }

	function get()
	{
		return extract($this->vars);
	}


	function render($filename)
	{
		$controller = strtolower(str_replace("Controller", "", get_class($this)));
		$url = PAGE_PATH.$controller.'/'.$filename.'.php';
		
		extract($this->vars);
		ob_start();
		if (isset($_SESSION['error'])){
			echo unserialize($_SESSION['error'])->display();
			$msg_error =  unserialize($_SESSION['error'])->display();
			unset($_SESSION['error']);
		} 

		
		//echo $msg_error;
		include $url;
		$content = ob_get_clean();
		return $content;
	}

	function url($controller, $action) {
		return WEBROOT.$controller."/" .$action;
	}

	function dispatch($controller, $action, $id=null, $param=array()) {
        // Instantiate the controller class and call its action method    
        //$error = new Error();
        //$this->error->setMsg($param['error']);
       

        $_SESSION['error'] = serialize($this->error);

       	header('Location: '.$this->url($controller, $action));
        return $controller->$action_name();

    }

    function error($error) {
    	//echo "<span>$error</span>";

    	/*$this->error = $error;
    	echo $this->error;*/
    }

	function __autoload($class_name) {
	   $class_name = strtolower($class_name);
	   $path       = "public/lib/{$class_name}.lib.php";
	   if (file_exists($path)) {
	       require_once($path);
	   } else {
	       die("The file {$class_name}.php could not be found!");
	   }
	}
}

?>