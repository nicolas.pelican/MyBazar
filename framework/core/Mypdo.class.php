<?php
class MyPDO
{
	protected $conn = false;  //DB connection resources

    protected $sql;           //sql statement

    protected $db;

    private $PDOInstance = null;
 
    private static $instance = null; 

    /**

     * Constructor, to connect to database, select database and set charset

     * @param $config string configuration array

     */

    private function __construct($config = array()){
        $dsn    =   isset($config['dsn'])?$config['dsn'] : '';

        $host = isset($config['host'])? $config['host'] : 'localhost';

        $user = isset($config['user'])? $config['user'] : 'root';

        $password = isset($config['password'])? $config['password'] : '';

        $dbname = isset($config['dbname'])? $config['dbname'] : '';

        $port = isset($config['port'])? $config['port'] : '3306';

        $charset = isset($config['charset'])? $config['charset'] : '3306';

        try {
            $this->db = new PDO($dsn, $user, $password);
        } catch (PDOException $e) {
            echo 'Connexion échouée : ' . $e->getMessage();
        }
    }


    //Singleton
     public static function getInstance($dbconfig) {
        if(is_null(self::$instance)) {
            self::$instance = new MyPDO($dbconfig); 
        }
        return self::$instance;
    }
     
    //----------------------------------------
    //FONCTIONS
    //----------------------------------------
     
    // Permet d'effectuer une requête SQL. Retourne le résultat (s'il y en a un) de la requête sous forme d'objet
    public function query($req){
        //$query = $this->db->query($req->queryString);
        $query = $this->db->query($req);
        return $query;
    }

    // Permet de préparer une requête SQL. Retourne la requête préparée sous forme d'objet
    public function prepare($req){

        $query = $this->db->prepare($req);
        return $query;
    }

    // Permet d'exécuter une requête SQL préparée. Retourne le résultat (s'il y en a un) de la requête sous forme d'objet
    public function execute($request){
        $request->execute();
    }

    // Retourne l'id de la dernière insertion par auto-incrément dans la base de données
    public function lastIndex(){
        return $this->db->lastInsertId();
    }

    public function fetchAll($request){
        return $request->fetchAll();
    }

}