<?php

Class View{
	
	public function url($module, $controller, $action) {
		if (file_exists(CONTROLLER_PATH . $controller. 'Controller')) {
			include($controller. 'Controller');
			$controller = new $controller. 'Controller';
			$controller->$action();
		}
	}

}