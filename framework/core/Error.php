<?php

namespace CORE;

class Error {
	 
	private static $instance = null; 

	protected $msg = '';
	protected $error;

	public function __construct()
    {
    }

	 function set($name, $value) {
	 	$this->$name = $value;
	 }

	 function get($name, $value) {
	 	return $this->$name;
	 }

 	public function setMsg($msg){

 		if (empty($this->msg)) {
				$this->msg = '<li>'.ucfirst($msg).'</li>'; 		
		} else {
			$this->msg .= '<li>'.ucfirst($msg).'</li>';
		}
 		/*
		foreach ($msg  as $value) {
			if (empty($this->msg)) {
				$this->msg = '<li>'.$value.'</li>'; 		
			} else {
				$this->msg .= '<li>'.$value.'</li>';
			}
		}*/
		
 	}

 	public function confirmPassword(string $str1, string $str2){
 		if ($str1 === $str2) {
 			return true;
 		}
 		return false;
 	}

 	public function checkFieldEmpty(array $fields) {
 		$error = array();
 		foreach ($fields as $key => $value) {
 			if (!isset($value{0})) {
 				$this->setMsg($key.' field is empty');
				/*array_push($error, $key.' field is empty');*/
			}	
		}
		//return($error);
 	}

 	public function checklenght(string $str, int $lenght) {
 		$error = array();
 		if (!isset($str{$lenght})) {
 			$this->setMsg($key.' must contain at least '.$lenght.' characters  ');
 			/*array_push($error, $key.' must contain at least '.$lenght.' characters  ');*/
 		}
 		//return $error;
 	}

 	public function getMsg(){
		return($this->msg); 		
 	}


 	public function display() {
 		if (!empty($this->msg)) {
	 		return('
	 		<ul class="breadcrumb msg-error">
	 			<ul>
	 				'.$this->msg.
	 			'</ul>
	 		</ul>');
 		} 
 	}
}