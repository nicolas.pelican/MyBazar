<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" type="text/css" href=<?php  echo(PROTOCOL . PUBLIC_PATH ."/css/bootstrap.min.css"); ?> />
        <link rel="stylesheet" type="text/css" href=<?php  echo(PROTOCOL . PUBLIC_PATH ."/css/style.css"); ?> />
        <script type="text/javascript" src="<?php  echo(PROTOCOL . PUBLIC_PATH); ?>js/jquery.min.js"></script>    
        <script type="text/javascript" src="<?php  echo(PROTOCOL . PUBLIC_PATH); ?>/js/bootstrap.min.js"></script>
         <script type="text/javascript" src="<?php  echo(PROTOCOL . PUBLIC_PATH); ?>/js/navigation.js"></script>
    </head>
    <body>
        <?php include('navbar.php'); ?>
        <div class="contenair">
            <div class="col-md-2 col-lg-2 col-sm-3">
                <?php include('menu.php'); ?>
            </div>
            <div class="col-md-9 col-lg-9" id="include">
                <?php echo $content; ?>
            </div>
        </div>
    </body>
</html>
