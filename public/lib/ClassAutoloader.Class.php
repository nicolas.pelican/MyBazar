<?php

namespace Bazar\Autoload;

class ClassAutoloader {
    public function __construct() {
        spl_autoload_register(array($this, 'loader'));
    }
    private function loader($className) {
        echo 'Trying to load ', $className, ' via ', __METHOD__, "()\n";
        include $className . '.php';
    }

    public function test() {
        echo "Ceci est un test";
    }
}

    $autoloader = new ClassAutoloader();

    $obj = new Class1();
    $obj = new Class2();
?>